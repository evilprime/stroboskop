# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://evilprime@bitbucket.org/evilprime/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/evilprime/stroboskop/commits/c09b8deb40d76da7a49cbddd59f320cae2f619dd?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/evilprime/stroboskop/commits/63fa622fa128caddee23a78397eca3dae0894907?at=master

Naloga 6.3.2:
https://bitbucket.org/evilprime/stroboskop/commits/51bfe30c7389ce3b86cee37e83ed4f5f08323465?at=master

Naloga 6.3.3:
https://bitbucket.org/evilprime/stroboskop/commits/cdcc352d1919da39ad0ae91a5f206406507de605?at=master

Naloga 6.3.4:
https://bitbucket.org/evilprime/stroboskop/commits/07fd6ed28a60a30cf2c8d0b2534ae3297822b9d4?at=master

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/evilprime/stroboskop/commits/1033c3836d173448a809c02309ab21b4638f45ed?at=master

Naloga 6.4.2:
https://bitbucket.org/evilprime/stroboskop/commits/bde1658cfd2ef5d24578eb2a047e1ad761b9925c?at=master

Naloga 6.4.3:
https://bitbucket.org/evilprime/stroboskop/commits/3e98eefe6b8016446b7385067cea6a9c5ada3955?at=master

Naloga 6.4.4:
https://bitbucket.org/evilprime/stroboskop/commits/2dc122c83f81a7c2342c9c8009abc9723c8a7109?at=master